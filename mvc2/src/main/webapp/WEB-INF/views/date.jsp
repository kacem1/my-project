<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Affecter les Dates de Soutenances</title>
<%-- <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/style1.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"/> --%>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}

img {
	max-width: 100%;
	max-height: 100%;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {
	height: 450px
}

/* Set gray background color and 100% height */
.sidenav {
	padding-top: 20px;
	background-color: #f1f1f1;
	height: 100%;
}

/* Set black background color, white text and some padding */
footer {
	background-color: #555;
	color: white;
	padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 15px;
	}
	.row.content {
		height: auto;
	}
}
</style>

</head>
<body>
	<div class="logo">
		<img id="u0_img" class="img "
			src="${pageContext.request.contextPath }/resources/photo_emiste_u0.png" />
	</div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">GPFE</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li><a href="acceuil">Accueil</a></li>
					<li><a href="ajouterEtu">Ajouter Etudiant</a></li>
					<li><a href="affecEnca">Affecter Encadrants Internes</a></li>
					<li><a href="affecJury">Affecter les Jurys</a></li>
					<li><a href="affecDate">Affecter les Dates de Soutenances</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>
							Login</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidenav">
				<p>
					<a href="#">Link</a>
				</p>
				<p>
					<a href="#">Link</a>
				</p>
				<p>
					<a href="#">Link</a>
				</p>
			</div>
			<div class="col-sm-8 text-left">
				<h1>Welcome</h1>
				<div>
					<form action="affecDate" method="post">
						<table>
							<tr>
								<td>Cherche eleve par matricule:</td>
								<td><input type="text" name="matricule" /></td>
								<td><input type="submit" name="action" value="Chercher" /></td>
							</tr>
						</table>
					</form>
				</div>

				<div>
					<table class="table">
						<tr>
							<th>Matricule</th>
							<th>Nom</th>
							<th>Prenom</th>
							<th>Email</th>
							<th>Sujet</th>
							<th>Organism Stage</th>
							<th>Encadrant Externe</th>
							<th>Encadrant Interne</th>
							<th>Jury 1</th>
							<th>Jury 2</th>
							<th>Jury 3</th>
							<th>Date Soutenance</th>
						</tr>
						<c:forEach items="${etudiantForm.etudiant }" var="e">
							<tr>
								<td>${e.matricule }</td>
								<td>${e.nom }</td>
								<td>${e.prenom }</td>
								<td>${e.email }</td>
								<td>${e.sujet }</td>
								<td>${e.organism_stage }</td>
								<td>${e.encadrant_extern }</td>
								<td>${e.encadrant_intern }</td>
								<td>${e.jury1 }</td>
								<td>${e.jury2 }</td>
								<td>${e.jury3 }</td>
								<td>${e.date }</td>
								<td><a href="affecterDate?type=${e.matricule }">Edit</a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<!-- 				<div> -->
				<form action="affecDate" method="post">
					<div>
						<button type="submit" name="action" value="Terminer"
							class="btn btn-primary">Terminer</button>
					</div>
				</form>
				<!-- 				</div> -->

			</div>
		</div>
	</div>
	<footer class="container-fluid text-center">
		<p>Footer Text</p>
	</footer>

</body>
</html>