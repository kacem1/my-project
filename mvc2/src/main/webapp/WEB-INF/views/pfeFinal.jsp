<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<html xmlns:th="www.thymleaf.org"
	xmlns:layout="http://www.ultraq.net.nz/thymleaf/layout"
	layout:decorator="layout">
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Informations personnelles</title>
<%-- <link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/resources/css/style1.css" /> --%>
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}

img{
	max-width: 100%;
	max-height: 100%;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {
	height: 450px
}

/* Set gray background color and 100% height */
.sidenav {
	padding-top: 20px;
	background-color: #f1f1f1;
	height: 100%;
}

/* Set black background color, white text and some padding */
footer {
	background-color: #555;
	color: white;
	padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 15px;
	}
	.row.content {
		height: auto;
	}
}
</style>
</head>
<body>
	<div class="logo">
		<img id="u0_img" class="img "
			src="${pageContext.request.contextPath }/resources/photo_emiste_u0.png" />
	</div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">GPFE</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout"><span class="glyphicon glyphicon-log-in"></span>
							Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			
			<div class="col-sm-8 text-left">
				<h2 style="margin-left: 10%;" >Profil</h2>
				<div class="container">
					<div align="center">
						<div class="spacer">
							<div class="col-md-6 col-sm-6 col-xs-12 spacer col-md-offset-3">
								<div class="panel panel-default">
									<f:form id="sendEmailForm" modelAttribute="etudiantForm"
										method="post" action="sendEmail" enctype="multipart/form-data">
										<c:forEach items="${etudiantForm.etudiant}" var="e">
											<table class="table table-striped">
												<tr>
													<td>Matricule :</td>
													<td>${e.matricule }</td>
												</tr>
												<tr>
													<td>Nom :</td>
													<td>${e.nom }</td>
												</tr>
												<tr>
													<td>Prenom :</td>
													<td>${e.prenom }</td>
												</tr>
												<tr>
													<td>Email :</td>
													<td>${e.email }</td>
												</tr>
												<tr>
													<td>Sujet :</td>
													<td>${e.sujet }</td>
												</tr>

												<tr>
													<td>Encadrant Externe :</td>
													<td>${e.encadrant_extern }</td>
												</tr>
												<tr>
													<td>Organism de stage :</td>
													<td>${e.organism_stage }</td>
												</tr>

											</table>

											<table id="exampleclass">
												<tr>
													<td></td>
													<td><input id="receiverMail" type="hidden"
														name="mailTo" size="65" value="${ e.email}" /></td>
													<!-- 	                    <td><input id="receiverMail2" type="hidden" name="mailTo2" size="65" value="dark-satin@live.fr"/></td> -->

												</tr>

												<tr>
													<td></td>
													<td><input id="mailSubject" type="hidden"
														name="subject" size="65" value="Informations Personnelles" /></td>
												</tr>
												<!-- 	                 <tr> -->
												<!-- 	                    <td></td> -->
												<%-- 	                    <td><input id="matricule" type="hidden" name="matricule" size="65" value=${e.matricule }/></td> --%>
												<!-- 	                </tr> -->

												<tr>
													<td></td>
													<td><textarea style="display: none;" id="mailMessage"
															cols="50" rows="10" name="message">Candidature Validé</textarea></td>
												</tr>
												<tr>

													<th>
													<input id="mailAttachment" type="file"
														name="attachFileObj" value="Upload file" size="60" /></th>
												</tr>
												<tr>
													<th colspan="2" align="center">
													<input name="action" id="sendEmailBtn" type="submit" value="Sauvegarder" />
													</th>

													<spring:url value="report/?type=${e.matricule }"
														var="pdfURL" />
													<spring:url value="/?type=${e.matricule }" var="type" />
													<p>
														<p><a href="${type }">Edit</a></p>
														<p><a href="${pdfURL }">Download PDF</a></p>
													</p>
												</tr>
											</table>


										</c:forEach>
									</f:form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				
			</div>

		</div>
	</div>

<footer class="container-fluid text-center">
                <p>Ecole Mohammadia d'Ingénieurs | Département Génie Informatique | 2018/2019</p>
	</footer>


</body>
</html>