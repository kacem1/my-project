<html xmlns:th="www.thymleaf.org"
	xmlns:layout="http://www.ultraq.net.nz/thymleaf/layout"
	layout:decorator="layout">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Ajouter Etudiant</title>
<%-- 
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"/> --%>
<style>
table {
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}

/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}

img {
	max-width: 100%;
	max-height: 100%;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {
	height: 450px
}

/* Set gray background color and 100% height */
.sidenav {
	padding-top: 20px;
	background-color: #f1f1f1;
	height: 150%;
}

/* Set black background color, white text and some padding */
footer {
	background-color: #555;
	color: white;
	padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 15px;
	}
	.row.content {
		height: auto;
	}
}
</style>

</head>
<body>
	<div class="logo">
		<img id="u0_img" class="img "
			src="${pageContext.request.contextPath }/resources/photo_emiste_u0.png" />
	</div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">GPFE</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li><a href="acceuil">Accueil</a></li>
					<li><a href="ajouterEtu">Ajouter Etudiant</a></li>
					<li><a href="affecEnca">Affecter Encadrants Internes</a></li>
					<li><a href="affecJury">Affecter les Jurys</a></li>
					<li><a href="affecDate">Affecter les Dates de Soutenances</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout"><span class="glyphicon glyphicon-log-in"></span>
							Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-8 text-left" style="margin-left: 10%">
				<h2>Ajouter Etudiant</h2>
				<div align="center">
					<div class="col-md-6 col-sm-6 col-xs-12 spacer col-md-offset-3">
						<div class="panel panel-default">
							<table class="table">
								<s:form commandName="etudiantData"
									action="${pageContext.request.contextPath }/ajouterEtu"
									method="post">
									<div style="border: none;" class="form-group">
										<label class="control-label">Matricule:</label>
										<input type="text" name="matricule" class="form-control" />
									</div>
									<div class="form-group">
										<label class="control-label">Nom:</label>
										<input type="text" name="nom" class="form-control" />
									</div>
									<div class="form-group">
										<label class="control-label">Prenom:</label> <input
											type="text" name="prenom" class="form-control" />
									</div>
									<div class="form-group">
										<label class="control-label">Email:</label> <input type="text"
											name="email" class="form-control" />
									</div>
									<div class="form-group">
										<label class="control-label">Sujet:</label> <input type="text"
											name="sujet" class="form-control" />
									</div>
									<div class="form-group">
										<label class="control-label">Organism de stage:</label> <select
											class="from-group" name="organism_stage">
											<option value="Organism1">Organism 1</option>
											<option value="Organism2">Organism 2</option>
											<option value="Organism3">Organism 3</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Encadrant externe:</label> <input
											type="text" name="encadrant_extern" class="form-control" />
									</div>
									<div>
										<button type="submit" class="btn btn-primary">Enregistrer</button>
									</div>
								</s:form>
							</table>

						</div>
					</div>
				</div>
				<hr>

			</div>

		</div>
	</div>
<footer class="container-fluid text-center">
                <p>Ecole Mohammadia d'Ingénieurs | Département Génie Informatique | 2018/2019</p>
	</footer>


</body>
</html>