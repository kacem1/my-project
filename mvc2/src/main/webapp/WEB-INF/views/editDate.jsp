<html xmlns:th="www.thymleaf.org"
	  xmlns:layout="http://www.ultraq.net.nz/thymleaf/layout"
	  layout:decorator="layout">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<%-- <%@page import="metier.EtudiantMetierImp" %> --%>

<head>
<meta charset="UTF-8">
<title>Affecter Date de Soutenance</title>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"/>
</head>
<body>
	<div align="center">
	<div class="col-md-6 col-sm-6 col-xs-12 spacer col-md-offset-3" >
	<div class="panel panel-default">
		<c:forEach items="${etudiantForm.etudiant}" var="e"	>		
		

<div>
<table class="table">
<s:form action="affecterDate" method="post">
				<div class="form-group">
					<label class="control-label">Matricule:</label>
					${ e.matricule}<input type="hidden" name="matricule" value="${ e.matricule}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Nom:</label>
					${ e.nom}<input type="hidden" name="nom" value="${ e.nom}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Prenom:</label>
					${ e.prenom}<input type="hidden" name="prenom" value="${ e.prenom}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Email:</label>
					${ e.email}<input type="hidden" name="email" value="${ e.email}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Sujet:</label>
					${ e.sujet}<input type="hidden" name="sujet" value="${ e.sujet}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Encadrant externe:</label>
					${ e.encadrant_extern}<input type="hidden" name="encadrant_extern" value="${ e.encadrant_extern}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Organism stage:</label>
					${ e.organism_stage}<input type="hidden" name="organism_stage" value="${ e.organism_stage}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Encadrant interne:</label>
					${ e.encadrant_intern}<input type="hidden" name="encadrant_intern" value="${ e.encadrant_intern}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Jury 1:</label>
					${ e.jury1}<input type="hidden" name="jury1" value="${ e.jury1}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Jury 1:</label>
					${ e.jury2}<input type="hidden" name="jury2" value="${ e.jury2}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Jury 3:</label>
					${ e.jury3}<input type="hidden" name="jury3" value="${ e.jury3}" class="form-control"/>
				</div>
				<div class="form-group">
					<label class="control-label">Date :</label>
					<input type="text" name="date" value="${ e.date}" class="form-control"/>
				</div>
				<div>
					<button type="submit" name="action" class="btn btn-primary">Save</button>
				</div>
<!-- 	<tr> -->
<!-- 	<td></td> -->
<!-- 	<td><input type="submit" name="action" value="Save"></td> -->
<!-- 	</tr> -->
</s:form>
</table>
</div>


</c:forEach>
</div>
</div>
</div>
</body>
</html>