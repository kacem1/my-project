<html xmlns:th="www.thymleaf.org"
	  xmlns:layout="http://www.ultraq.net.nz/thymleaf/layout"
	  layout:decorator="layout">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<head>
<meta charset="utf-8">
<style>
body {font-family: Arial, Helvetica, sans-serif;}


input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 16px;
  width: 40%;
  margin: auto;
  
}
img{
	max-width: 30%;
	max-height: 30%;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
<title>Login</title>
</head>
<body>
  <div class="imgcontainer">
      <img id="u8" 
      class="img " 
      src="${pageContext.request.contextPath }/resources/logo.png"
      style="text-align: center;"
      height="30%"
      width="20%"
      />
  </div>
	<s:form commandName="etudiantData" action="${pageContext.request.contextPath }/logina" method="post" >
  <div class="container">
    <label for="uname"><b>Matricule</b></label>
    <input type="text" placeholder="Enter Username" name="matricule" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
    
        
    <button type="submit">Login</button>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
    <span class="psw"><a href="#">Forgot password?</a></span>
  </div>
			</s:form>
		  <div><p style="color:red;">${failed }</p></div>

</body>
</html>