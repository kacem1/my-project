package org.gestion.mvc.models;

import java.util.List;

import javax.validation.constraints.Size;

import org.gestion.mvc.entities.Etudiant;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.domain.Page;

public class EtudiantForm {

	@NotEmpty
	@Size(min=4,max=7)
	private String code;
	private List<Etudiant> etudiant;
	private Page<Etudiant> etudiants;
	public Page<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(Page<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	public void setEncadrant_intern(String encadrant_intern) {
		this.encadrant_intern = encadrant_intern;
	}

	private String email;
	private String sujet;
	private String encadrant_intern;
	private String encadrant_extern;
	private String organism_stage;
	private String data;
	


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public String getEncadrant_intern() {
		return encadrant_intern;
	}

	public void setEncadrant_inter(String encadrant_intern) {
		this.encadrant_intern = encadrant_intern;
	}

	public String getEncadrant_extern() {
		return encadrant_extern;
	}

	public void setEncadrant_extern(String encadrant_extern) {
		this.encadrant_extern = encadrant_extern;
	}

	public String getOrganism_stage() {
		return organism_stage;
	}

	public void setOrganism_stage(String organism_stage) {
		this.organism_stage = organism_stage;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public List<Etudiant> getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(List<Etudiant> etudiant) {
		this.etudiant = etudiant;
	}

	private String exception;
	
	
	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
