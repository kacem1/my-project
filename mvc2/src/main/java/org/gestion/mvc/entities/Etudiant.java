package org.gestion.mvc.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

	@Entity
public class Etudiant implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id ;
    @NotEmpty

	private String matricule ;
	private String nom ;
	private String prenom ;
	private String email ;
	private String sujet ;
	private String encadrant_intern ;
	private String encadrant_extern;
	private String organism_stage;
	private String type;
	private String jury1;
	private String jury2;
	private String jury3;
//	@DateTimeFormat(pattern="yyyy-MM-dd")
	private String date;
    public String getJury1() {
		return jury1;
	}









	public String getDate() {
		return date;
	}









	public void setDate(String date) {
		this.date = date;
	}









	public void setJury1(String jury1) {
		this.jury1 = jury1;
	}

	public String getJury2() {
		return jury2;
	}

	public void setJury2(String jury2) {
		this.jury2 = jury2;
	}

	public String getJury3() {
		return jury3;
	}

	public void setJury3(String jury3) {
		this.jury3 = jury3;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public Etudiant(String matricule, String nom, String prenom, String email, String sujet, 
			String organism_stage,String encadrant_extern, String type, String password) {
		super();
		this.matricule = matricule;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.sujet = sujet;
		this.organism_stage = organism_stage;
		this.encadrant_extern = encadrant_extern;
		this.type = type;
		this.password = password;
	}


	@NotEmpty

	private String password;
	
	@Override
	public String toString() {
		return " Ana etudiant [matricule=" + matricule + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email
				+ ", sujet=" + sujet + ", encadrant_intern=" + encadrant_intern + ", encadrant_extern="
				+ encadrant_extern + ", organism_stage=" + organism_stage + ", password=" + password + "]";
	}

	// constructeur sans parametres pour JPA et moi aussi
	public Etudiant() {
		super();
		
	}

	public Etudiant(String matricule, String nom, String prenom, String email, String sujet, String encadrant_intern,
			String encadrant_extern, String organism_stage, String password, String type) {
		super();
		this.matricule = matricule;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.sujet = sujet;
		this.encadrant_intern = encadrant_intern;
		this.encadrant_extern = encadrant_extern;
		this.organism_stage = organism_stage;
		this.password = password;
		this.type = type;
	}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public String getEncadrant_intern() {
		return encadrant_intern;
	}

	public void setEncadrant_intern(String encadrant_intern) {
		this.encadrant_intern = encadrant_intern;
	}

	public String getEncadrant_extern() {
		return encadrant_extern;
	}

	public void setEncadrant_extern(String encadrant_extern) {
		this.encadrant_extern = encadrant_extern;
	}

	public String getOrganism_stage() {
		return organism_stage;
	}

	public void setOrganism_stage(String organism_stage) {
		this.organism_stage = organism_stage;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
