package org.gestion.mvc.dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.gestion.mvc.entities.Etudiant;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public class EtudiantDaoImpl implements IEtudiantDao{
	@PersistenceContext
	private EntityManager em;
	@Autowired
    private SessionFactory sessionFactory;
     
    public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public List<Etudiant> getEleveParMatricule(String matricule) {
		/*Etudiant e=em.find(Etudiant.class, matricule);
		if(e==null) throw new RuntimeException("Etudiant introuvable");
		return e;*/
		
		Query req=em.createQuery("select e from Etudiant e where e.matricule like :x");
		req.setParameter("x",matricule);
		return req.getResultList();
	}

	@Override
	public Etudiant addEtudiant(Etudiant e) {
		em.persist(e);
		return e;
	}

	@Override
	public void updateEtudiant(String matricule, String email, String sujet, String encadrant_extern, String organism_stage) {
		
		Query req=em.createQuery("update Etudiant set email=?, sujet=?, encadrant_extern=?, organism_stage=? where matricule=?");
		req.setParameter(1, email);
		req.setParameter(2, sujet);
//		req.setParameter(3, encadrant_intern);
		req.setParameter(3, encadrant_extern);
		req.setParameter(4, organism_stage);
//		req.setParameter(6, data);
		req.setParameter(5, matricule);
		req.executeUpdate();
		//return null;
	}

	@Override
    public Etudiant loginEtudiant(Etudiant etudiant) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        String hql = "from org.gestion.mvc.entities.Etudiant as c where c.matricule =? and c.password =?";
        try {
            org.hibernate.Query query = session.createQuery(hql);
            query.setParameter(0, etudiant.getMatricule());
            query.setParameter(1, etudiant.getPassword());
            etudiant = (Etudiant) query.uniqueResult();
            tx.commit();
            session.close();
        } catch (Exception e) {
            tx.rollback();
            session.close();
            e.printStackTrace();
        }
        return etudiant;
    }
	@Override
	public List<Etudiant> listEtudiants() {
		Query req=em.createQuery("select e from Etudiant e where e.type NOT IN('admin')");
//		req.setParameter("x",matricule);
		return req.getResultList();
		
	}
	@Override
	public void deleteEtudiant(String matricule) {
		List<Etudiant> e= getEleveParMatricule(matricule);
		
		em.remove(e.get(0));
		
	}
	@Override
	public void updateEncadrant(String matricule, String encadrant_intern) {
		Query req=em.createQuery("update Etudiant set encadrant_intern=? where matricule=?");
		req.setParameter(1, encadrant_intern);
		req.setParameter(2, matricule);
		req.executeUpdate();
		//return null;
	}
	@Override
	public void updateJury(String matricule, String jury1, String jury2, String jury3) {
		Query req=em.createQuery("update Etudiant set jury1=?,jury2=?,jury3=? where matricule=?");
		req.setParameter(1, jury1);
		req.setParameter(2, jury2);
		req.setParameter(3, jury3);
		req.setParameter(4, matricule);
		req.executeUpdate();
		
	}
	@Override
	public void updateEdit(String matricule, String nom, String prenom, String email, String sujet,
			String organism_stage, String encadrant_extern) {
		Query req=em.createQuery("update Etudiant set nom=?, prenom=?,email=?,sujet=?, organism_stage=?,encadrant_extern=? where matricule=?");
		req.setParameter(1, nom);
		req.setParameter(2, prenom);
		req.setParameter(3, email);
		req.setParameter(4, sujet);
		req.setParameter(5, organism_stage);
		req.setParameter(6, encadrant_extern);
		req.setParameter(7, matricule);
		req.executeUpdate();
		//return null;
		
	}
	@Override
	public void updateDate(String matricule, String date) {
		Query req=em.createQuery("update Etudiant set date=? where matricule=?");
		req.setParameter(1, date);
		req.setParameter(2, matricule);
		req.executeUpdate();
		
	}
	@Override
	public Page<Etudiant> listEtudiant(Pageable pageable) {
		Query req=em.createQuery("select e from Etudiant e where e.type NOT IN('admin')");
		return (Page<Etudiant>)req.getResultList();
//		req.setParameter("x",matricule);
//		return req.getSingleResult();
	}
	@Override
	public List<Etudiant> listCandidature() {
		Query req=em.createQuery("select e from Etudiant e where e.type NOT IN('admin') and e.sujet NOT IN('') ");
//		req.setParameter("x",matricule);
		return req.getResultList();
	}
	
		
	}


	


