package org.gestion.mvc.controllers;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.persistence.metamodel.SetAttribute;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.gestion.mvc.entities.Etudiant;
import org.gestion.mvc.metier.IEtudiantMetier;
import org.gestion.mvc.models.EtudiantForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;





@Controller
public class EtudiantController {


	static String emailToRecipient,emailToRecipient2, emailSubject, emailMessage;
	static final String emailFromRecipient = "majdoubhoussam@gmail.com";

	static ModelAndView modelViewObj;

	@Autowired
	private JavaMailSender mailSenderObj;
	
	@Autowired
	private IEtudiantMetier metier;

	
	@RequestMapping(value="/", method=RequestMethod.GET)

		public String listNotes(HttpServletRequest req, HttpServletResponse res, EtudiantForm bf,Model model) {
		  String typeReport = req.getParameter("type");
		  List<Etudiant> cp=metier.getEleveParMatricule(typeReport);

			bf.setEtudiant(cp);
			
		model.addAttribute("etudiantForm", bf);
		if(cp.isEmpty()) return "login";
		else {
			return "etudiantinfo"; 
		}
		}

	@RequestMapping(value="pfe")
	public String chargerFinal(EtudiantForm bf,Model model) {
		
		
		metier.updateEtudiant(bf.getCode(), bf.getEmail(), bf.getSujet(), bf.getEncadrant_extern(), bf.getOrganism_stage());
		List<Etudiant> cp=metier.getEleveParMatricule(bf.getCode());
		bf.setEtudiant(cp);
		model.addAttribute("etudiantForm", bf);
		return "pfeFinal"; //il doit chercher une page jsp qui s'appelle banque
	}
	
	@RequestMapping(value = "sendEmail", method = RequestMethod.POST)
	public String sendEmailToClient(HttpServletRequest request, final @RequestParam CommonsMultipartFile attachFileObj,HttpServletResponse res) {

		// Reading Email Form Input Parameters		
		emailSubject = request.getParameter("subject");
		emailMessage = request.getParameter("message");
		emailToRecipient = request.getParameter("mailTo");
//		emailToRecipient2 = request.getParameter("mailTo2");

		// Logging The Email Form Parameters For Debugging Purpose
//		System.out.println("\nReceipient?= " + emailToRecipient + ", Subject?= " + emailSubject + ", Message?= " + emailMessage + "\n");
//userListReport(request, res);
//		res.setContentType("application/pdf");
		mailSenderObj.send(new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper mimeMsgHelperObj = new MimeMessageHelper(mimeMessage, true, "UTF-8");				
//				BodyPart messagebodypart=new MimeBodyPart();
				
//				mimeMsgHelperObj.setTo(emailToRecipient2);
				mimeMsgHelperObj.setTo(emailToRecipient);
				mimeMsgHelperObj.setFrom(emailFromRecipient);				
				mimeMsgHelperObj.setText(emailMessage); //utf-8   informations personnelles
				mimeMsgHelperObj.setSubject(emailSubject);
				String filename= "C:\\Oppa.pdf";
//				/NewEleve/src/main/webapp/WEB-INF/Pdf.pdf
				DataSource source = new FileDataSource(filename);
				mimeMsgHelperObj.addAttachment("Oppa.pdf", source);
//				mimeMsgHelperObj.add
//				Transport.send(mimeMessage);
//				  String masterPath = req.getSession().getServletContext().getRealPath("/WEB-INF/Pdf.pdf");

//			}
//		});
//				attachFileObj.s
//				mimeMsgHelperObj.addAttachment
				// Determine If There Is An File Upload. If Yes, Attach It To The Client Email				
				if ((attachFileObj != null) && (attachFileObj.getSize() > 0) && (!attachFileObj.equals(""))) {
					System.out.println("\nAttachment Name?= " + attachFileObj.getOriginalFilename() + "\n");
					mimeMsgHelperObj.addAttachment(attachFileObj.getOriginalFilename(), new InputStreamSource() {					
						public InputStream getInputStream() throws IOException {
							return attachFileObj.getInputStream();

						}
					});
				} else {
					System.out.println("\nNo Attachment Is Selected By The User. Sending Text Email!\n");
				}
			}
		});
		System.out.println("\nMessage Send Successfully.... Hurrey!\n");

		modelViewObj = new ModelAndView("success","messageObj","Thank You! Your Email Has Been Sent!");
		return  "login";	
	}
		
	
	@RequestMapping(value="/report", method=RequestMethod.GET)
	 public void userListReport(HttpServletRequest req, HttpServletResponse res)throws ServerException, IOException{
		  
		  String typeReport = req.getParameter("type");

	List<Etudiant> cp=metier.getEleveParMatricule(typeReport);

	  
	  String masterPath = req.getSession().getServletContext().getRealPath("/WEB-INF/Pdf.pdf");
	  
	  res.setContentType("application/pdf");
	  try ( PdfReader reader = new PdfReader( masterPath );
			  PdfWriter writer = new PdfWriter(res.getOutputStream());
			  PdfDocument document = new PdfDocument(reader, writer)  ){
		  
		  PdfPage page = document.getPage(1);
		  PdfCanvas canvas = new PdfCanvas(page);
		  
		   
		  FontProgram fontProgram = FontProgramFactory.createFont();
		  PdfFont font = PdfFontFactory.createFont(fontProgram,"utf-8",true);
		  canvas.setFontAndSize(font, 12);
		  
		  canvas.beginText();
		  canvas.setTextMatrix(0,0);
		  canvas.showText(""+cp.get(0).getMatricule()	);
		  canvas.setTextMatrix(420,550);
		  canvas.showText(""+cp.get(0).getMatricule()	);
		  canvas.setTextMatrix(420,510);
		  canvas.showText(""+cp.get(0).getNom()	);
		  canvas.setTextMatrix(420,470);
		  canvas.showText(""+cp.get(0).getPrenom()	);
		  canvas.setTextMatrix(420,430);
		  canvas.showText(""+cp.get(0).getEmail()	);
		  canvas.setTextMatrix(420,390);
		  canvas.showText(""+cp.get(0).getSujet()	);
		  canvas.setTextMatrix(420,350);
		  canvas.showText(""+cp.get(0).getEncadrant_extern()	);
		  canvas.setTextMatrix(420,310);
		  canvas.showText(""+cp.get(0).getOrganism_stage());
		  canvas .endText();
		  
		  
	  }
	  

		 }
	    
		 @RequestMapping(value = "logina", method = RequestMethod.POST)
     public String doLogin(HttpServletRequest req, HttpServletResponse res,Locale locale, ModelMap model,@ModelAttribute("etudiantData")Etudiant etudiant, HttpSession session, EtudiantForm bf) {

		List<Etudiant> cp=metier.getEleveParMatricule(etudiant.getMatricule());
    	bf.setEtudiant(cp);
		model.addAttribute("etudiantForm", bf);
    	 if(etudiant.getMatricule()!=null && etudiant.getPassword()!=null && session.getAttribute("etudiant")==null) {
    		etudiant=metier.loginEtudiant(etudiant);
    		if(etudiant!=null) {
    			if(cp.get(0).getType().equals("superadmin"))	{
    				List<Etudiant> cpp=metier.listEtudiants();
    				
    				bf.setEtudiant(cpp);
    				model.addAttribute("etudiantForm", bf);
    				return 
    						"redirect:http://localhost:8087/Spring4MVCFileUploadDownloadWithHibernate/";}
    			if(cp.get(0).getType().equals("admin"))	{
    				List<Etudiant> cpp=metier.listEtudiants();
    				
    				bf.setEtudiant(cpp);
    				model.addAttribute("etudiantForm", bf);
    				return 
    						"approch";}
    			
    			
    			
    			
    			
    			
    			
    			else {
    			if(cp.get(0).getSujet().equals("")) {
    			session.setAttribute("etudiant", etudiant);
    			return "etudiantinfo";
    		}
    			else {
    					return "pfeFinal";
    			}
    			}
    			}
    		else {
    			model.put("failed", "Login Failed");
    			return "login";	
    		}
    	}else {
    		return "etudiantinfo";
    	}
    }
    
	 @RequestMapping(value="suivantt", method = RequestMethod.GET)
		public String graphic() {
	    	return "suivant";
	 }
	 
	 @RequestMapping(value="logout", method = RequestMethod.GET)
		public String logout() {
	    	return "login";
	 } 
		@RequestMapping(value="chercher", method = RequestMethod.POST)
		public String chercher(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			String action=req.getParameter("action");
			if(action!=null) {
				if(action.equals("Ajouter")) {
				String matr=req.getParameter("matricule");
				String nom=req.getParameter("nom");
				String prenom=req.getParameter("prenom");
				String email=req.getParameter("email");
				String sujet=req.getParameter("sujet");
				String organism=req.getParameter("organism_stage");
				String enca=req.getParameter("encadrant_extern");
				
				Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,organism,enca,"etudiant",matr);
//				metier.updateEdit(matr, nom, prenom, email, sujet, organism, enca);
				metier.addEtudiant(et);
				List<Etudiant> cpp=metier.listEtudiants();		
				bf.setEtudiant(cpp);
				model.addAttribute("etudiantForm", bf);
			}
				else if(action.equals("Chercher")) {
			String mat = req.getParameter("matricule");
			if(mat.equals("")) {
			List<Etudiant> cpp=metier.listEtudiants();		
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			}
			else {
			List<Etudiant> cp=metier.getEleveParMatricule(mat);
			
			bf.setEtudiant(cp);
			model.addAttribute("etudiantForm", bf);
			}
				}
			 
		}
			return "suivant";
		}

		
		@RequestMapping(value="chercher", method = RequestMethod.GET)
		public String cherchers(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			String action = req.getParameter("action");
			if(action!=null) {
				if(action.equals("delete")) {
					String typeReport = req.getParameter("type");
					metier.deleteEtudiant(typeReport);
			
			List<Etudiant> cpp=metier.listEtudiants();
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			
		}
				else if(action.equals("edit")) {
					String typeReport = req.getParameter("type");
					List<Etudiant> cp=metier.getEleveParMatricule(typeReport);
					Etudiant etu=cp.get(0);
					model.addAttribute("etu", etu);
					List<Etudiant> cpp=metier.listEtudiants();
					bf.setEtudiant(cpp);
					model.addAttribute("etudiantForm", bf);
				}
				
				else if(action.equals("affecEnca")) {
					String typeReport = req.getParameter("type");
					List<Etudiant> cp=metier.getEleveParMatricule(typeReport);
					Etudiant etu=cp.get(0);
					model.addAttribute("etu", etu);
					List<Etudiant> cpp=metier.listEtudiants();
					bf.setEtudiant(cpp);
					model.addAttribute("etudiantForm", bf);
				}


			}return "suivant";
		}	
		
		@RequestMapping(value="editEtudiant",method=RequestMethod.GET)
		public String editEtu(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			  String typeReport = req.getParameter("type");
			  List<Etudiant> cp=metier.getEleveParMatricule(typeReport);

				bf.setEtudiant(cp);
				
			model.addAttribute("etudiantForm", bf);
			return "editEtu";
		}
		
		@RequestMapping(value="editEtudiant",method=RequestMethod.POST)
		public String editEtud(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			String matricule=req.getParameter("matricule");
			String email=req.getParameter("email");
			String sujet=req.getParameter("sujet");
			String encadrant_extern=req.getParameter("encadrant_extern");
			String organism_stage=req.getParameter("organism_stage");
//			Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,enca,organism,"etudiant",matr);
//			metier.updateEncadrant(matricule,encadrant_intern);
			metier.updateEtudiant(matricule, email, sujet, encadrant_extern, organism_stage);
			List<Etudiant> cpa=metier.listEtudiants();		
			bf.setEtudiant(cpa);
			model.addAttribute("etudiantForm", bf);
			return "suivant";
		}
		@RequestMapping(value="affecEncadrant",method=RequestMethod.GET)
		public String Affecter(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			  String typeReport = req.getParameter("type");
			  List<Etudiant> cp=metier.getEleveParMatricule(typeReport);

				bf.setEtudiant(cp);
				
			model.addAttribute("etudiantForm", bf);
			return "editEn";
		}
		@RequestMapping(value="affecEncadrant",method=RequestMethod.POST)
		public String Affecterr(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			String matricule=req.getParameter("matricule");
			
			String encadrant_intern=req.getParameter("encadrant_intern");
//			Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,enca,organism,"etudiant",matr);
			metier.updateEncadrant(matricule,encadrant_intern);
			List<Etudiant> cpa=metier.listEtudiants();		
			bf.setEtudiant(cpa);
			model.addAttribute("etudiantForm", bf);
			return "enca";
		}
		@RequestMapping(value="affecterJury",method=RequestMethod.GET)
		public String Affecterju(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			  String typeReport = req.getParameter("type");
			  List<Etudiant> cp=metier.getEleveParMatricule(typeReport);

				bf.setEtudiant(cp);
				
			model.addAttribute("etudiantForm", bf);
			return "editJu";
		}
		@RequestMapping(value="affecterJury",method=RequestMethod.POST)
		public String Affecterj(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			String matricule=req.getParameter("matricule");
			String jury1=req.getParameter("jury1");
			String jury2=req.getParameter("jury2");
			String jury3=req.getParameter("jury3");
//			Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,enca,organism,"etudiant",matr);
			metier.updateJury(matricule,jury1,jury2,jury3);
			List<Etudiant> cpa=metier.listEtudiants();		
			bf.setEtudiant(cpa);
			model.addAttribute("etudiantForm", bf);
			return "jury";
		}
		
		@RequestMapping(value="affecEnca", method=RequestMethod.GET)
		public String affecterEncadrant(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			return "enca";
		}
		
		@RequestMapping(value="affecEnca", method = RequestMethod.POST)
		public String suivantt(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			String action = req.getParameter("action");
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			
			if(action!=null) {
				if(action.equals("Modifier")) {
					String matricule=req.getParameter("matricule");
					
					String encadrant_intern=req.getParameter("encadrant_intern");
//					Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,enca,organism,"etudiant",matr);
					metier.updateEncadrant(matricule,encadrant_intern);
					List<Etudiant> cpa=metier.listEtudiants();		
					bf.setEtudiant(cpa);
					model.addAttribute("etudiantForm", bf);
					
				}
			
				else if(action.equals("Chercher")) {
					String mat = req.getParameter("matricule");
					if(mat.equals("")) {
					List<Etudiant> cop=metier.listEtudiants();		
					bf.setEtudiant(cop);
					model.addAttribute("etudiantForm", bf);
					}
					else {
						List<Etudiant> cp=metier.getEleveParMatricule(mat);
						
						bf.setEtudiant(cp);
						model.addAttribute("etudiantForm", bf);
						}


			}
						 
		}	return "enca";
		}
		
		@RequestMapping(value="ajouterEtu", method=RequestMethod.POST)
		public String AjouterEtudiants(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			String matr=req.getParameter("matricule");
			String nom=req.getParameter("nom");
			String prenom=req.getParameter("prenom");
			String email=req.getParameter("email");
			String sujet=req.getParameter("sujet");
			String organism=req.getParameter("organism_stage");
			String enca=req.getParameter("encadrant_extern");
			
			Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,organism,enca,"etudiant",matr);
//			metier.updateEdit(matr, nom, prenom, email, sujet, organism, enca);
			metier.addEtudiant(et);
			List<Etudiant> cpp=metier.listEtudiants();		
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			return "suivant";
		}
		@RequestMapping(value="ajouterEtu", method=RequestMethod.GET)
		public String AjouterEtudiant(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			return "ajoutE";
		}
		
//		@RequestMapping(value="affecEnca", method = RequestMethod.GET)
//		public String suivants(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
//			
//			String action = req.getParameter("action");
//			
//			if(action.equals("edit")) {
//				String typeReport = req.getParameter("type");
//				List<Etudiant> cp=metier.getEleveParMatricule(typeReport);
//				Etudiant etu=cp.get(0);
//				model.addAttribute("etu", etu);
//				List<Etudiant> cap=metier.listEtudiants();
//				bf.setEtudiant(cap);
//				model.addAttribute("etudiantForm", bf);
//			}
//			
//			else if(action.equals("affecjury")) {
//				String typeReport = req.getParameter("type");
//				List<Etudiant> cp=metier.getEleveParMatricule(typeReport);
//				Etudiant etu=cp.get(0);
//				model.addAttribute("etu", etu);
//				List<Etudiant> cpp=metier.listEtudiants();
//				bf.setEtudiant(cpp);
//				model.addAttribute("etudiantForm", bf);
//			}
//
//
//		
//			return "enca";
//		}	
		@RequestMapping(value="affecJury", method = RequestMethod.POST)
		public String jury(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			String action = req.getParameter("action");
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			
			if(action!=null) {
				if(action.equals("Modifier")) {
					String matricule=req.getParameter("matricule");
					String jury1=req.getParameter("jury1");
					String jury2=req.getParameter("jury2");
					String jury3=req.getParameter("jury3");
//					Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,enca,organism,"etudiant",matr);
					metier.updateJury(matricule,jury1,jury2,jury3);
					List<Etudiant> cpa=metier.listEtudiants();		
					bf.setEtudiant(cpa);
					model.addAttribute("etudiantForm", bf);
					
				}
			
				else if(action.equals("Chercher")) {
					String mat = req.getParameter("matricule");
					if(mat.equals("")) {
					List<Etudiant> cop=metier.listEtudiants();		
					bf.setEtudiant(cop);
					model.addAttribute("etudiantForm", bf);
					}
					else {
						List<Etudiant> cp=metier.getEleveParMatricule(mat);
						
						bf.setEtudiant(cp);
						model.addAttribute("etudiantForm", bf);
						}


			}
						 
		}	return "jury";
		}
			
		
		@RequestMapping(value="affecJury", method = RequestMethod.GET)
		public String jurye(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
//			return "enca";
		
			return "jury";
		}	
		
		@RequestMapping(value="affecDate", method = RequestMethod.POST)
		public String date(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			
			String action = req.getParameter("action");
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			
			if(action!=null) {
				if(action.equals("Modifier")) {
					String matricule=req.getParameter("matricule");
					String date=req.getParameter("date");
//					Etudiant et=new Etudiant(matr,nom,prenom,email,sujet,enca,organism,"etudiant",matr);
					metier.updateDate(matricule, date);
					List<Etudiant> cpa=metier.listEtudiants();		
					bf.setEtudiant(cpa);
					model.addAttribute("etudiantForm", bf);
					
				}
				else if(action.equals("Terminer")) {
					return "login";}
			
				else if(action.equals("Chercher")) {
					String mat = req.getParameter("matricule");
					if(mat.equals("")) {
				List<Etudiant> cop=metier.listEtudiants();		
					bf.setEtudiant(cop);
					model.addAttribute("etudiantForm", bf);
					}
					else {
						List<Etudiant> cp=metier.getEleveParMatricule(mat);
						
						bf.setEtudiant(cp);
						model.addAttribute("etudiantForm", bf);
						}


			}
						 
		}	return "date";
		}
			
		@RequestMapping(value="affecterDate",method=RequestMethod.POST)
		public String Affecterdaa(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			String matricule=req.getParameter("matricule");
			String date=req.getParameter("date");
			metier.updateDate(matricule, date);
			List<Etudiant> cpa=metier.listEtudiants();		
			bf.setEtudiant(cpa);
			model.addAttribute("etudiantForm", bf);
			
			return "date";
		}
		
		@RequestMapping(value="acceuil", method=RequestMethod.GET)
		public String accueil(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			return "suivant";
		}
		
		@RequestMapping(value="affecDate", method=RequestMethod.GET)
		public String affecterDate(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			List<Etudiant> cpp=metier.listEtudiants();
			
			bf.setEtudiant(cpp);
			model.addAttribute("etudiantForm", bf);
			return "date";
		}
		
		@RequestMapping(value="affecterDate",method=RequestMethod.GET)
		public String Affecterda(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf,Model model) {
			  String typeReport = req.getParameter("type");
			  List<Etudiant> cp=metier.getEleveParMatricule(typeReport);

				bf.setEtudiant(cp);
				
			model.addAttribute("etudiantForm", bf);
			return "editDate";
		}
		
		@RequestMapping(value="Etudiant", method=RequestMethod.GET)
		public void op(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf) {
				
			try {
			HSSFWorkbook workbook= new HSSFWorkbook();
			CellStyle header =workbook.createCellStyle();
			HSSFFont font= workbook.createFont();
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.DARK_GREEN.index);
			
			header.setFont(font);
			header.setAlignment(CellStyle.ALIGN_CENTER);
			
			HSSFSheet etudiant =workbook.createSheet("Etudiant data");
			HSSFRow row=etudiant.createRow(0);
			HSSFCell cell;
			
			cell=row.createCell(0);
			cell.setCellStyle(header);
			cell.setCellValue("Matricule");
			
			cell=row.createCell(1);
			cell.setCellStyle(header);
			cell.setCellValue("Nom");
			
			cell=row.createCell(2);
			cell.setCellStyle(header);
			cell.setCellValue("Prenom");
			
			cell=row.createCell(3);
			cell.setCellStyle(header);
			cell.setCellValue("Email");
			
			cell=row.createCell(4);
			cell.setCellStyle(header);
			cell.setCellValue("Encadrant externe");
			
			int rowIndex=1;
			List<Etudiant> list = metier.listEtudiants();
			for(int i=0;i<list.size();i++) {
				row=etudiant.createRow(rowIndex++);
				cell=row.createCell(0);
				cell.setCellValue(list.get(i).getMatricule());
				
				cell=row.createCell(1);
				cell.setCellValue(list.get(i).getNom());
				
				cell=row.createCell(2);
				cell.setCellValue(list.get(i).getPrenom());
				
				cell=row.createCell(3);
				cell.setCellValue(list.get(i).getEmail());
				
				cell=row.createCell(4);
				cell.setCellValue(list.get(i).getEncadrant_extern());
				
			}
			
			res.setContentType("application/vnd.ms-excel");
			try(OutputStream out=res.getOutputStream()){
				workbook.write(out);
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		@RequestMapping(value="Candidature", method=RequestMethod.GET)
		public void cnd(HttpServletRequest req, HttpServletResponse res,EtudiantForm bf) {
				
			try {
			HSSFWorkbook workbook= new HSSFWorkbook();
			CellStyle header =workbook.createCellStyle();
			HSSFFont font= workbook.createFont();
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.DARK_GREEN.index);
			
			header.setFont(font);
			header.setAlignment(CellStyle.ALIGN_CENTER);
			HSSFSheet etudiant =workbook.createSheet("Candidature data");
			HSSFRow row=etudiant.createRow(0);
			HSSFCell cell;
			
			cell=row.createCell(0);
			cell.setCellStyle(header);
			cell.setCellValue("Matricule");
			
			cell=row.createCell(1);
			cell.setCellStyle(header);
			cell.setCellValue("Nom");
			
			cell=row.createCell(2);
			cell.setCellStyle(header);
			cell.setCellValue("Prenom");
			
			cell=row.createCell(3);
			cell.setCellStyle(header);
			cell.setCellValue("Email");
			
			cell=row.createCell(4);
			cell.setCellStyle(header);
			cell.setCellValue("Encadrant externe");
			
			cell=row.createCell(5);
			cell.setCellStyle(header);
			cell.setCellValue("Sujet");

			cell=row.createCell(6);
			cell.setCellStyle(header);
			cell.setCellValue("Organisme stage");
			
			int rowIndex=1;
			List<Etudiant> list = metier.listCandidature();
			for(int i=0;i<list.size();i++) {
				row=etudiant.createRow(rowIndex++);
				cell=row.createCell(0);
				cell.setCellValue(list.get(i).getMatricule());
				
				cell=row.createCell(1);
				cell.setCellValue(list.get(i).getNom());
				
				cell=row.createCell(2);
				cell.setCellValue(list.get(i).getPrenom());
				
				cell=row.createCell(3);
				cell.setCellValue(list.get(i).getEmail());
				
				cell=row.createCell(4);
				cell.setCellValue(list.get(i).getEncadrant_extern());

				cell=row.createCell(5);
				cell.setCellValue(list.get(i).getSujet());
				
				cell=row.createCell(6);
				cell.setCellValue(list.get(i).getOrganism_stage());
				
				
			}
			
			res.setContentType("application/vnd.ms-excel");
			try(OutputStream out=res.getOutputStream()){
				workbook.write(out);
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		
			
			
		}
		
		
}


			