package org.gestion.mvc.metier;


import java.util.List;

import org.gestion.mvc.dao.IEtudiantDao;
import org.gestion.mvc.entities.Etudiant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
@Transactional
public class EtudiantMetierImpl implements IEtudiantMetier{

	private IEtudiantDao dao;
	
	public void setDao(IEtudiantDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Etudiant> getEleveParMatricule(String matricule) {
		// TODO Auto-generated method stub
		return dao.getEleveParMatricule(matricule);
	}

	@Override
	public void updateEtudiant(String matricule, String email, String sujet, String encadrant_extern, String organism_stage) {
		// TODO Auto-generated method stub
		 dao.updateEtudiant(matricule, email, sujet, encadrant_extern, organism_stage);
	}

	@Override
	public Etudiant addEtudiant(Etudiant e) {
		// TODO Auto-generated method stub
		return dao.addEtudiant(e);
	}

	@Override
	public Etudiant loginEtudiant(Etudiant etudiant) {
		// TODO Auto-generated method stub
		return dao.loginEtudiant(etudiant);
	}

	@Override
	public List<Etudiant> listEtudiants() {
		// TODO Auto-generated method stub
		return dao.listEtudiants();
	}

	@Override
	public void deleteEtudiant(String matricule) {
		dao.deleteEtudiant(matricule);
		
	}

	@Override
	public void updateEncadrant(String matricule, String encadrant_intern){
		dao.updateEncadrant(matricule, encadrant_intern);
		
	}

	@Override
	public void updateJury(String matricule, String jury1, String jury2, String jury3) {
		dao.updateJury(matricule, jury1, jury2, jury3);
		
	}

	@Override
	public void updateEdit(String matricule, String nom, String prenom, String email, String sujet,
			String organism_stage, String encadrant_extern) {
		dao.updateEdit(matricule, nom, prenom, email, sujet, organism_stage, encadrant_extern);
		
	}

	@Override
	public void updateDate(String matricule, String date) {
		dao.updateDate(matricule, date);
		
	}

	@Override
	public Page<Etudiant> listEtudiant(Pageable pageable) {
		// TODO Auto-generated method stub
		return dao.listEtudiant(pageable);
	}

	@Override
	public List<Etudiant> listCandidature() {
		// TODO Auto-generated method stub
		return dao.listCandidature();
	}

	

}
