package org.gestion.mvc.metier;


import java.util.List;

import org.gestion.mvc.entities.Etudiant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface IEtudiantMetier {

	public List<Etudiant> getEleveParMatricule(String matricule);
//	public List<Etudiant> getElevesParMatricule(String matricule, Pageable pageable);
	public void updateEtudiant(String matricule, String email, String sujet, String encadrant_extern, String organism_stage);
	public Etudiant addEtudiant(Etudiant e);
	public Etudiant loginEtudiant(Etudiant etudiant);
	public List<Etudiant> listEtudiants();
	public Page<Etudiant> listEtudiant(Pageable pageable);
	public void deleteEtudiant(String matricule);
	public void updateEncadrant(String matricule, String encadrant_intern);
	public void updateDate(String matricule, String date);
	public void updateJury(String matricule, String jury1, String jury2, String jury3);
	public void updateEdit(String matricule,String nom,String prenom,String email,String sujet,String organism_stage,String encadrant_extern);
	public List<Etudiant> listCandidature();

}
