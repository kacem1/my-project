<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>

<html>
<head>

<title>Login</title>
</head>
<body>
	<div align="center">
		<h1>Login</h1>
		<table>
			<s:form commandName="etudiantData" action="${pageContext.request.contextPath }/logina" method="post">
			<tr>
				<td>Matricule:</td>
				<td><input name="matricule"/></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input name="password" type="password"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Login"/></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			</s:form>
		</table>
		<div></div>
		<div>
		<p style="color:red;">${failed }</p>
		</div>
		
	</div>

</body>
</html>