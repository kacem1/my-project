<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html"; charset="ISO-8859-1">
<title>Informations personnelles</title>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/style1.css"/>

</head>
<body>
<div>
<f:form id="sendEmailForm"  modelAttribute="etudiantForm" method="post" action="sendEmail" enctype="multipart/form-data"> 
		<c:forEach items="${etudiantForm.etudiant}" var="e"	>
		<table class="table1" border="1" width="40%">

				<tr>
					<td>Matricule :</td>  
					<td>${e.matricule }</td>
				</tr>
				<tr>
					<td>Nom :</td>  
					<td>${e.nom }</td>
				</tr>
				<tr>
					<td>Prenom :</td>  
					<td>${e.prenom }</td>
				</tr>
				<tr>
					<td>Email :</td>  
					<td>${e.email }</td>
				</tr>
				<tr>
					<td>Sujet :</td>  
					<td>${e.sujet }</td>
				</tr>

				<tr>
					<td>Encadrant Externe :</td>  
					<td>${e.encadrant_extern }</td>
				</tr>
				<tr>
					<td>Organism de stage :</td>  
					<td>${e.organism_stage }</td>
				</tr>

			</table>
			
			        <table id="exampleclass" >
	                <tr>
	                    <td></td>
	                    <td><input id="receiverMail" type="hidden" name="mailTo" size="65" value="${ e.email}"/></td>
<!-- 	                    <td><input id="receiverMail2" type="hidden" name="mailTo2" size="65" value="dark-satin@live.fr"/></td> -->

	                </tr>
	                
	                <tr>
	                    <td></td>
	                    <td><input id="mailSubject" type="hidden" name="subject" size="65" value="Informations Personnelles"/></td>
	                </tr>
<!-- 	                 <tr> -->
<!-- 	                    <td></td> -->
<%-- 	                    <td><input id="matricule" type="hidden" name="matricule" size="65" value=${e.matricule }/></td> --%>
<!-- 	                </tr> -->
	                
	                <tr>
	                    <td></td>
	                    <td><textarea style="display: none;" id="mailMessage" cols="50" rows="10" name="message" >Candidature Validé</textarea></td>
	                </tr>
	                <tr>
	                    <td></td>
	                    <td><input id="mailAttachment" type="file" name="attachFileObj" value="" size="60" /></td>
	                </tr>
	                <tr>
	                <td colspan="2" align="center"><input name="action" id="sendEmailBtn" type="submit" value="Terminer" /></td>
	                     
	                		 <spring:url value="report/?type=${e.matricule }" var="pdfURL" />
	                		 <spring:url value="/?type=${e.matricule }" var="type" />
	                		 <td><a href="${type }">Edit</a></td>
 <td><a href="${pdfURL }">Download PDF</a></td>
<!--  <td><a href="logout">Terminer</a></td> -->
	                </tr>
	            </table>


	</c:forEach>
</f:form>
</div>
</body>
</html>