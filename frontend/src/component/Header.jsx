import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AuthenticationService from './AuthenticationService.js'

import logo from "./emi.png";
class HeaderComponent extends Component {
    render() {
            const isUserLoggedIn = AuthenticationService.isUserLoggedIn();

        return (
             <header>

                            <nav className="shadow-lg navbar navbar-expand-sm bg-primary navbar-dark">

                                <ul className="navbar-nav">
                                    {isUserLoggedIn && <li><Link className="nav-link" to="/Home/:name">
                                    <span className="icon">
                                    <i className="fa fa-home" ></i>
                                    </span>
                                    Home</Link></li>}
                                    {isUserLoggedIn && <li><Link className="nav-link" to="/AllApprenant">Apprenant</Link></li>}
                                    {isUserLoggedIn && <li><Link className="nav-link" to="/AllCours">Cours</Link></li>}
                                      {isUserLoggedIn && <li><Link className="nav-link" to="/AllEnseignant">Enseignant</Link></li>}

                                </ul>
                                <ul className="navbar-nav navbar-collapse justify-content-end">
                                    {!isUserLoggedIn && <li><Link className="nav-link" to="/login">Login</Link></li>}
                                    {isUserLoggedIn && <li><Link className="nav-link" to="/logout" onClick={AuthenticationService.logout}>Logout</Link></li>}
                                </ul>
                            </nav>
                        </header>
        )
            ;
    }
}

export default HeaderComponent;