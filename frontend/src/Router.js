import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HeaderComponent from './component/Header';
import FooterComponent from './component/foter';
import ApprenantComponent from "./component/Apprenant/Apprennant";
import CoursComponent from './component/Cours/Cours';
import EnseignantComponent from './component/Enseignant/Enseignant';
import HomeComponent from './component/Home'
import FoterComponent from './component/foter';
import EditCoursComponent from './component/Cours/EditCoursComponent';
import EditApprenantComponent from './component/Apprenant/EditApprenantComponent';
import EditEnseignantComponent from './component/Enseignant/EditEnseignantComponent';


import CreatCoursComponent from './component/Cours/CreateCoursComponent';
import DeleteCoursComponent from './component/Cours/DeletCoursComponent';
import DeleteEnseignantComponent from './component/Enseignant/DeleteEnseignant';
import CreatEnseignantComponent from './component/Enseignant/CreatEnseignantComponent';
import CreateApprenantComponent from './component/Apprenant/CreatApprenantComponent';
import affectationCoursComponent from './component/adhésion_cours/affectaionCoursCompoment';
import cour_to_apprenant from './component/adhésion_cours/cours_to_apprenant';
import LoginComponent from './component/LoginComponent'
import LogoutComponent from './component/LogoutComponent'
import AuthenticatedRoute from './component/AuthenticatedRoute.jsx'

import DeleteApprenantComponent from './component/Apprenant/DeleteApprenantComponent';

class RouterComponent extends Component {
    render() {
        return (
            <Router>
                <>
                <HeaderComponent/>
                    <br></br>
                    <Switch>

                        <Route path="/" exact component={LoginComponent}/>
                        <Route path="/login" component={LoginComponent}/>
                          <Route path="/logout" component={LogoutComponent}/>

                        <AuthenticatedRoute   path="/Home/:name"  component={HomeComponent} />
                        
                        <AuthenticatedRoute  exact  path="/AllApprenant"   component={ApprenantComponent} />
                        <AuthenticatedRoute   exact path="/AllCours"   component={CoursComponent} />
                        <AuthenticatedRoute  exact path="/AllEnseignant"  component={EnseignantComponent} />
                        
                        <AuthenticatedRoute exact path="/EditCour/:id"  component={EditCoursComponent} />
                        <AuthenticatedRoute  exact  path="/EditApprenant/:id" component={EditApprenantComponent} />
                        <AuthenticatedRoute  exact path="/EditEnseignant/:id" component={EditEnseignantComponent}></AuthenticatedRoute >

                        <AuthenticatedRoute  exact  path="/CreateApprenant" component={CreateApprenantComponent} />
                        <AuthenticatedRoute  exact path="/CreateCours" component={CreatCoursComponent}></AuthenticatedRoute >
                        <AuthenticatedRoute  exact path="/CreateEnseignant" component={CreatEnseignantComponent}></AuthenticatedRoute >
                        
                        <AuthenticatedRoute   exact path="/deleteEnseignant/:id" component={DeleteEnseignantComponent}></AuthenticatedRoute >
                        <AuthenticatedRoute   exact path="/deleteCour/:id" component={DeleteCoursComponent}></AuthenticatedRoute >
                        <AuthenticatedRoute   exact path="/deleteApprenant/:id" component={DeleteApprenantComponent}></AuthenticatedRoute >

                        <AuthenticatedRoute   exact path="/Apprenant/:id/Cours" component={affectationCoursComponent}></AuthenticatedRoute >
                        <AuthenticatedRoute   exact path="/Apprenant/:id/affectaionCours" component={cour_to_apprenant}></AuthenticatedRoute >

                    </Switch>
                    <br></br>
                    {

}
                  <FooterComponent/>
                </>

            </Router>
        )
    }
}

export default RouterComponent;